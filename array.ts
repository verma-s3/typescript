//first way to define array 

var list:number[] = [1,2,3,4,5];

var books:string[]= ['Math','English','Hindi'];

console.log(list);

console.log(books);

//second way fo rdefining array 

var list1:Array<number> = [10,52,45,56,78];
var name1:Array<string> = ['SOnia','Nisha','Jiya','Daksh'];

console.log(list1);
console.log(name1);

//use of touple

var employee:[number,string]=[1,'SOnia'];

console.log(employee);

employee = [1,'Sonia']; // this will override the employee touple
console.log(employee);

employee.push(2,'Nisha');// adding more values to touple
employee.push(3,'Jiya');
console.log(employee);

//other way of using touple

var empid:number =2;
var empname:string='Nisha';

var emp:[number, string] = [empid, empname];
console.log(emp);


//Array inside touple

var user:[number,string,string][];

user = [[1,"Sunny",'Admin'],[2,'Sonia','Developer'],[3,'Iqbal','Programmer']];
console.log(user);

//USe of generic Datatype

function show1<T>(a:T):T {
    return a;
}

var output1 = show1<string>("SOnia");
var output2 = show1<number>(10);

console.log(output1);
console.log(output2);
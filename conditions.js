//decaration of number array
// var list:Array<number> = [12,23,12,45,45,78,89];
// for(let i=0;i<list.length;i++){
//     document.writeln(list[i]);
// }
var list1 = [12, 12, 3, 35, 21];
// for (let x=0;x<list1.length;x++){
//     document.writeln(list1[x]);
// }
for (var val in list1) {
    console.log(list1);
}
// function with optional parameters
function greet(name, greeting) {
    if (greeting == undefined) {
        document.writeln("Hi " + name + "\n");
    }
    else {
        document.writeln("\n" + greeting + " " + name);
    }
}
greet("Sonia");
greet("Sunny", "Good Morning");

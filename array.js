//first way to define array 
var list = [1, 2, 3, 4, 5];
var books = ['Math', 'English', 'Hindi'];
console.log(list);
console.log(books);
//second way fo rdefining array 
var list1 = [10, 52, 45, 56, 78];
var name1 = ['SOnia', 'Nisha', 'Jiya', 'Daksh'];
console.log(list1);
console.log(name1);
//use of touple
var employee = [1, 'SOnia'];
console.log(employee);
employee = [1, 'Sonia']; // this will override the employee touple
console.log(employee);
employee.push(2, 'Nisha'); // adding more values to touple
employee.push(3, 'Jiya');
console.log(employee);
//other way of using touple
var empid = 2;
var empname = 'Nisha';
var emp = [empid, empname];
console.log(emp);
//Array inside touple
var user;
user = [[1, "Sunny", 'Admin'], [2, 'Sonia', 'Developer'], [3, 'Iqbal', 'Programmer']];
console.log(user);
//USe of generic Datatype
function show(a) {
    return a;
}
var output1 = show("SOnia");
var output2 = show(10);
console.log(output1);
console.log(output2);
